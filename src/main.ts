import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './scss/main.scss';
import store from './store';
import Header from '@/components/Header.vue';
import './registerServiceWorker';

import VueFire from 'vuefire'

Vue.use(VueFire)

Vue.config.productionTip = false;

Vue.component('app-header', Header);

Vue.component('v-input', (resolve) => {
  require(['@/components/Vinput'], resolve);
});

Vue.component('table-data', (resolve) => {
  require(['@/components/TableData'], resolve);
});

// Vue.component('main-map', (resolve) => {
//   require(['@/components/Map'], resolve);
// });

Vue.component('main-plan', (resolve) => {
  require(['@/components/Plan'], resolve);
});

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app');
